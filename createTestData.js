import { createClient } from '@clickhouse/client';
import { DateTime } from 'luxon';

const secondsInADay = 86400;

async function createEntryWithOffset(client, id, createdAt, offsetInSeconds) {
  let timeInSeconds = 0

  while (timeInSeconds < secondsInADay) {
    await client.exec({
      query: `
      INSERT INTO test (id, created_at) Values (${id}, '${createdAt.toFormat('yyyy-MM-dd HH:mm:ss')}');
      `,
      clickhouse_settings: {
        wait_end_of_query: 1,
      },
    })

    id++;
    timeInSeconds += offsetInSeconds;
    createdAt = createdAt.plus({seconds: offsetInSeconds});
  }

  return {id, createdAt};
}

void (async () => {
  const client = createClient({
    host: 'http://localhost:18123',
  })

  await client.ping();

  await client.exec({
    query: `
      CREATE OR REPLACE TABLE test
      (
          id UInt64,
          created_at DateTime,
      )
      ENGINE = MergeTree
      ORDER BY id;
    `,
    clickhouse_settings: {
      wait_end_of_query: 1,
    },
  });


  let id = 1;
  // Start from 1970-02-01 just so we don't have any less than 0 issues
  let createdAt = DateTime.fromMillis(secondsInADay * 31 * 1000, {zone: 'utc'});

  console.log('Starting from', createdAt.toFormat('yyyy-MM-dd HH:mm:ss'));

  // Every 2 minutes (30 per hour)
  ({id, createdAt} = await createEntryWithOffset(client, id, createdAt, 120));
  // Every minutes (60 per hour)
  ({id, createdAt} = await createEntryWithOffset(client, id, createdAt, 60));
  // Every 30 seconds (120 per hour)
  ({id, createdAt} = await createEntryWithOffset(client, id, createdAt, 30));

  await client.close()
})();
