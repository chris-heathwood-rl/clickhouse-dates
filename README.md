# clickhouse dates

```sh
docker compose up
```

```sh
npm install
```

```sh
node createTestData.js
```

## Some bits of SQL

```sql
-- Create or replace the table
CREATE OR REPLACE TABLE test
(
    id UInt64,
    created_at DateTime,
)
ENGINE = MergeTree
ORDER BY id;
```

```sql
-- Check it has created ok
SELECT * FROM test;
```

```sql
-- Insert now just to check it works
INSERT INTO test (id, created_at) Values (1, now());
```

```sql
-- GROUP by date using UTC
SELECT
    toDate(created_at, 'UTC') AS groupByDate,
    count()
FROM
    test
GROUP BY
    groupByDate;
```

```sql
-- GROUP by date for one date using UTC
SELECT
    toDate(created_at, 'UTC') AS groupByDate,
    count()
FROM
    test
WHERE
	created_at >= toDateTime('1970-02-02 00:00:00', 'UTC') AND
	created_at < toDateTime('1970-02-03 00:00:00', 'UTC')
GROUP BY
    groupByDate;
```

```sql
-- GROUP by date using US/Samoa (GMT-11)
-- Check calculations
--   Every 2 minutes (30 per hour)
--     30 * 11 = 330
--   Every minutes (60 per hour)
--     (720 - 330) + (60 * 11) = 390 + 660 = 1050
--   Every 30 seconds (120 per hour)
--     (1440 - 660) + (120 * 11) = 780 + 1320 = 2100
--   Left over
--     2880 - 1320 = 1560
SELECT
    toDate(created_at, 'US/Samoa') AS groupByDate,
    count()
FROM
    test
GROUP BY
    groupByDate;
```

```sql
-- GROUP by date for one date using US/Samoa (GMT-11)
SELECT
    toDate(created_at, 'US/Samoa') AS groupByDate,
    count()
FROM
    test
WHERE
	created_at >= toDateTime('1970-02-02 00:00:00', 'US/Samoa') AND
	created_at < toDateTime('1970-02-03 00:00:00', 'US/Samoa')
GROUP BY
    groupByDate;
```